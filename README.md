# TigerBeer

## Framework: NuxtJS/VueJS

```bash
# Cài đặt dependencies
$ npm install

# Serve folder ở host localhost:8080
$ npm run dev

# Build và deploy web lên host
$ npm run build:deploy
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

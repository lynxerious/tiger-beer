const functions = require("firebase-functions");

exports.social = functions.https.onRequest((req, res) => {
  console.log(
    "social",
    req.headers["user-agent"],
    req.headers["user-agent"].includes("facebookexternalhit"),
    req.query
  );
  const redirect = `https://rinhbiavenha.vn?source=${req.query["source"]}`;
  if (req.headers["user-agent"].includes("facebookexternalhit")) {
    res.status(200).send(`
    <html>
        <head>
            <title>Bật Tiger, Khui bản lĩnh. Dũng mãnh đương đầu.</title>
            <meta name="description" content="Bật Tiger, Khui bản lĩnh. Dũng mãnh đương đầu.">
            <!-- You can use Open Graph tags to customize link previews.
            Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
            <meta  property="og:type" content="website">
            <meta  property="og:title" content="Rinh Bia Về Nhà">
            <meta  property="og:description" content="${req.query["source"]}">
            <meta  property="og:image" content="https://i.imgur.com/YEyP4hv.jpg">
        </head>
        <body></body>
    </html>
    `);
  } else {
    res.redirect(redirect);
  }
});
